import React from 'react'
import PropTypes from 'prop-types'
import {css, StyleSheet} from 'aphrodite'
import autobind from 'autobind-decorator'
import {font} from '../lib/styles'
import {size} from '../config/size'
import {withProps} from '../lib/decorators'
import {DropTarget} from 'react-dnd'

const target = {
  canDrop(props, monitor) {
    return (
      !props.value.value && props.value.type.includes(monitor.getItem().type)
    )
  },

  drop(props, monitor) {
    const partId = monitor.getItem().id
    const slot = props.value.name
    const targetId = props.parentId
    props.onInsert({slot, partId, targetId})
  }
}

function collectDrop(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop()
  }
}

@DropTarget('part', target, collectDrop)
@autobind
export default class Slot extends React.Component {
  static propTypes = {
    value: PropTypes.object,
    parentId: PropTypes.string,
    onInsert: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
  }

  @withProps
  removePart({onRemove, name, parentId, value}) {
    const slot = name
    const sourceId = parentId
    const partId = value.value
    onRemove({slot, partId, sourceId})
  }

  @withProps
  render({value, isDragging, canDrop, connectDropTarget}) {
    return connectDropTarget(
      <div
        onClick={value.value && this.removePart}
        className={css(
          styles.root,
          isDragging && styles._dragged,
          canDrop ? styles._valid_target : value.value ? styles._filled : styles._empty
        )}
      >
        {value.name}
      </div>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    ...font,
    marginBottom: 5,
    padding: 2,
    fontSize: 10,
    width: size.part.slot,
    minWidth: size.part.slot,
    height: 12,
    borderWidth: 1,
    borderStyle: 'solid'
  },

  _dragged: {
    opacity: 0.2
  },

  _valid_target: {
    borderColor: '#fb7000'
  },

  _filled: {
    borderColor: '#5400dd'
  },

  _empty: {
    borderColor: '#ddd'
  },

  type: {
    ...font,
    fontSize: 10
  }
})

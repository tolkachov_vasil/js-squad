import PropTypes from 'prop-types'
import React from 'react'
import {css, StyleSheet} from 'aphrodite'

const Col = ({
               className,
               larger,
               right,
               gapLeft,
               gapRight,
               expanded,
               children
             }) => {
  const cn = css(
    styles.root,
    larger && styles.larger,
    right && styles.right,
    gapLeft && styles.gapLeft,
    gapRight && styles.gapRight,
    expanded && styles.expanded
  )

  return className ? (
    <div className={className}>
      <div className={cn}>{children}</div>
    </div>
  ) : (
    <div className={cn}>{children}</div>
  )
}

const styles = StyleSheet.create({
  root: {
    flexShrink: 1
  },

  larger: {
    flexGrow: 0.5
  },

  expanded: {
    flexGrow: 1,
    width: 0
  },

  right: {
    textAlign: 'right'
  },

  gapLeft: {
    marginLeft: 10
  },

  gapRight: {
    marginRight: 10
  }
})

Col.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
  right: PropTypes.bool,
  larger: PropTypes.bool,
  gapLeft: PropTypes.bool,
  gapRight: PropTypes.bool,
  expanded: PropTypes.bool
}

export default Col

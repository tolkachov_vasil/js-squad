import PropTypes from 'prop-types'
import React from 'react'
import {css, StyleSheet} from 'aphrodite'

const Row = ({className, wrap, start, children}) => {
  const cn = css(styles.root, wrap && styles.wrap, start && styles.start)

  return className ? (
    <div className={className}>
      <div className={cn}>{children}</div>
    </div>
  ) : (
    <div className={cn}>{children}</div>
  )
}

const styles = StyleSheet.create({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'nowrap'
  },
  wrap: {
    flexWrap: 'wrap'
  },
  start: {
    justifyContent: 'flex-start'
  }
})

Row.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
  wrap: PropTypes.bool,
  start: PropTypes.bool
}

export default Row

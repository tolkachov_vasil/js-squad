import React from 'react'
import PropTypes from 'prop-types'
import {css, StyleSheet} from 'aphrodite'
import autobind from 'autobind-decorator'
import {withProps} from '../lib/decorators'
import AceEditor from 'react-ace'
import 'brace/mode/html'
import 'brace/mode/json'
import '../config/ace-snippets'
import 'brace/ext/language_tools'
import 'brace/theme/github'
import 'firacode/distr/fira_code.css'
import {View3D} from './3d/View3D'
import pretty from 'pretty'
import ReactJson from 'react-json-view'
import Part3D from './3d/Part3D'

const JSON_STYLE = {fontSize: 14, fontFamily: 'Fira Code'}

const ACE_OPTIONS = {
  enableBasicAutocompletion: true,
  enableLiveAutocompletion: true,
  enableSnippets: true,
  showLineNumbers: true,
  showGutter: true,
  tabSize: 2,
  fontFamily: 'Fira Code',
  fontSize: 14
}

@autobind
export default class PartEdit extends React.Component {
  static propTypes = {
    part: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
  }

  @withProps
  autoFormat({part}) {
    const model = pretty(part.model || '', {ocd: true})
    this.updateModel(model)
  }

  @withProps
  update({onChange, part: {id, originId, model}}, {updated_src}) {
    onChange({...updated_src, id, originId, model})
  }

  @withProps
  addChild({onChange, part}) {
    const child = part.child || {}
    const name = 'child_' + Object.keys(child).length

    onChange({
      ...part,
      child: {
        ...child,
        [name]: {
          name,
          capacity: 1,
          pos: '0 0 0',
          type: 'type'
        }
      }
    })
  }

  @withProps
  updateModel({onChange, part}, model) {
    onChange({...part, model})
  }

  @withProps
  save({onSave, part}) {
    onSave(part)
  }

  @withProps
  delete({onDelete, part}) {
    onDelete(part.id)
  }

  @withProps
  render({part}) {
    const {model, id, originId, ...clean} = part

    return (
      <div className={css(styles.root)}>
        <ReactJson
          src={clean}
          theme='github'
          iconStyle='triangle'
          style={JSON_STYLE}
          collapsed
          displayObjectSize={false}
          displayDataTypes={false}
          enableClipboard={false}
          onEdit={this.update}
          onAdd={this.update}
          onDelete={this.update}
        />
        <button onClick={this.addChild}>add child</button>
        <br/>
        <AceEditor
          mode="html"
          theme="github"
          name="model"
          height='10rem'
          width='100%'
          wrapEnabled
          onChange={this.updateModel}
          highlightActiveLine
          value={model}
          // editorProps={{$blockScrolling: Infinity}}
          setOptions={ACE_OPTIONS}/>
        <br/>
        <button onClick={this.autoFormat}>format</button>
        <button onClick={this.save}>save</button>
        <button onClick={this.delete}>delete</button>
        <br/>
        <View3D>
          {id && <Part3D value={part} noChild axes/>}
        </View3D>
      </div>
    )
  }
}

const styles = StyleSheet.create({
  root: {}
})

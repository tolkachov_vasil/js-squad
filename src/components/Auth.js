import React from 'react'
import {connect} from 'react-redux'
import {withProps} from '../lib/decorators'
import {userLoadProfileRequest, userLoginRequest, userLogoutRequest} from '../redux/actions/user'
import {hardReload} from '../lib/utils'
import {assetsInit} from '../redux/actions/assets'

@connect(
  ({user}) => ({user}),
  {
    userLoginRequest,
    userLogoutRequest,
    userLoadProfileRequest,
    assetsInit
  })
export default class Auth extends React.Component {

  @withProps
  render({user, children, userLoginRequest, userLogoutRequest}) {
    return (
      <div>
        <button onClick={hardReload}> Reset</button>
        {user.logged
          ? this.renderAuthorized()
          : this.renderUnauthorized()
        }
      </div>
    )
  }

  @withProps
  renderUnauthorized({user, children, userLoginRequest}) {
    return (
      <button onClick={userLoginRequest}> Login </button>
    )
  }

  @withProps
  renderAuthorized({user, children, userLogoutRequest}) {
    return (
      <span>
        {user.role} - {user.displayName}
        <img src={user.photoURL}/>
        <button onClick={userLogoutRequest}> Logout </button>
        {children}
      </span>
    )
  }
}

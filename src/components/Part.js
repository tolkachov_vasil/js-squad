import React from 'react'
import PropTypes from 'prop-types'
import {css, StyleSheet} from 'aphrodite'
import autobind from 'autobind-decorator'
import {font} from '../lib/styles'
import Row from './common/Row'
import {size} from '../config/size'
import {withProps} from '../lib/decorators'
import {DragSource} from 'react-dnd'
import Slot from './Slot'

const source = {
  beginDrag(props) {
    props.onSelect(props.part.id)
    return props.part
  },

  endDrag(props, monitor) {
    // eslint-disable-next-line
    const item = monitor.getItem()
    // eslint-disable-next-line
    const dropResult = monitor.getDropResult()
  }
}

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  }
}

@DragSource('part', source, collect)
@autobind
export default class Part extends React.Component {
  static propTypes = {
    part: PropTypes.object,
    selected: PropTypes.bool,
    onInsert: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired
  }

  @withProps
  insertPart({onInsert}, {slot, partId, targetId}) {
    onInsert(targetId, partId, slot)
  }

  @withProps
  removePart({onRemove}, {slot, partId, sourceId}) {
    onRemove(sourceId, partId, slot)
  }

  @withProps
  select({onSelect, part}) {
    onSelect(part.id)
  }

  @withProps
  render({part, isDragging, selected, connectDragSource}) {
    return connectDragSource(
      <div
        onClick={this.select}
        className={css(
          styles.root,
          isDragging && styles._dragged,
          selected && styles._selected
        )}>
        <div className={css(styles.type)}>{part.type}</div>
        <div className={css(styles.name)}>{part.name}</div>
        {part.child && this.renderSlots(part)}
        {this.renderStats(part)}
      </div>
    )
  }

  renderStats({stats, baseStats}) {
    const items = stats || baseStats || {}
    return (
      <div className={css(styles.stats)}>
        {Object.keys(items).map(k => (
          <div key={k} className={css(styles.stats__item)}>
            {k}: {items[k]}
          </div>
        ))}
      </div>
    )
  }

  renderSlots({child = {}, id}) {
    return (
      <Row wrap className={css(styles.slots)}>
        {Object.values(child).map(slot => (
          <Slot
            onInsert={this.insertPart}
            onRemove={this.removePart}
            value={slot}
            name={slot.name}
            parent={id}
            key={slot.name}
          />
        ))}
      </Row>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    width: size.part.side,
    height: 'fit-content',
    minWidth: size.part.side,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#aaa',
    borderRadius: 6,
    margin: 5,
    padding: 5,
    cursor: 'pointer'
  },

  _selected: {
    borderColor: '#000'
  },

  _dragged: {
    opacity: 0.2
  },

  slots: {
    marginBottom: 5
  },

  type: {
    ...font,
    fontSize: 10,
    marginTop: -5
  },

  name: {
    ...font,
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5
  },

  stats__item: {
    ...font,
    fontSize: 12
  }
})

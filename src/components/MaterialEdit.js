import React from 'react'
import PropTypes from 'prop-types'
import {css, StyleSheet} from 'aphrodite'
import autobind from 'autobind-decorator'
import {withProps} from '../lib/decorators'
import 'firacode/distr/fira_code.css'
import {View3D} from './3d/View3D'
import ReactJson from 'react-json-view'

const JSON_STYLE = {fontSize: 14, fontFamily: 'Fira Code'}

@autobind
export default class MaterialEdit extends React.Component {
  static propTypes = {
    value: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
  }

  @withProps
  update({onChange}, {updated_src}) {
    onChange({...updated_src})
  }

  @withProps
  save({onSave, value}) {
    onSave(value)
  }

  @withProps
  delete({onDelete, value}) {
    onDelete(value.name)
  }

  @withProps
  render({value}) {

    return (
      <div className={css(styles.root)}>
        <ReactJson
          src={value}
          theme='github'
          iconStyle='triangle'
          style={JSON_STYLE}
          collapsed
          displayObjectSize={false}
          displayDataTypes={false}
          enableClipboard={false}
          onEdit={this.update}
          onAdd={this.update}
          onDelete={this.update}
        />
        <br/>
        <button onClick={this.save}>save</button>
        <button onClick={this.delete}>delete</button>
        <br/>
        <View3D>
          {value.name && <a-box {...value}></a-box>}
        </View3D>
      </div>
    )
  }
}

const styles = StyleSheet.create({
  root: {}
})

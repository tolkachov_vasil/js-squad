import React from 'react'
import {css, StyleSheet} from 'aphrodite'
import autobind from 'autobind-decorator'
import {connect} from 'react-redux'
import Row from './common/Row'
import {withProps} from '../lib/decorators'
import Part from './Part'
import {DragDropContext} from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import {partsAttach, partsDetach} from '../redux/actions/parts'
import {selectPart} from '../redux/actions/select'

@DragDropContext(HTML5Backend)
@connect(
  ({parts, select}) => ({parts, select}),
  {partsAttach, partsDetach, selectPart})
@autobind
export default class Parts extends React.Component {

  @withProps
  render({parts, select, partsAttach, partsDetach, selectPart}) {
    const selectedId = select.part

    return (
      <Row start wrap className={css(styles.root)}>
        {Object.values(parts)
          .map(p => (
            <Part
              key={p.id}
              part={p}
              selected={p.id === selectedId}
              onInsert={partsAttach}
              onRemove={partsDetach}
              onSelect={selectPart}
            />
          ))}
      </Row>
    )
  }
}

const styles = StyleSheet.create({
  root: {}
})

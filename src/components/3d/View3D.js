import PropTypes from 'prop-types'
import React, {Component} from 'react'
import autobind from 'autobind-decorator'
import {withPropsAndState} from '../../lib/decorators'

const COCKPIT_SCALE = 0.3

@autobind
export class View3D extends Component {
  static propTypes = {
    cockpit: PropTypes.any
  }

  state = {
    canvasRef: null,
    width: window.innerWidth - 20,
    height: window.innerWidth / 2
  }

  componentWillMount() {
    window.addEventListener('resize', this.resize, false)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize, false)
  }

  resize() {
    this.setState({
      width: window.innerWidth - 20,
      height: window.innerWidth / 2
    })
  }

  @withPropsAndState
  render({children, cockpit}, {width, height}) {
    return (
      <a-scene
        embedded
        style={{width, height, background: 'rgb(255, 255, 255)'}}
      >
        <a-camera
          active
          position="0 1 3"
          orbit-controls="target: #oc-target; enable-damping: true; damping-factor: 0.125; rotate-speed:0.25; rotate-to-speed: 0.04;"
          mouse-cursor
        >
          <a-light intensity="2" position="0 0 1" color="#fff"/>
          <a-entity position='0 -0.6 -1' scale={`${COCKPIT_SCALE} ${COCKPIT_SCALE} ${COCKPIT_SCALE}`}>
            {cockpit}
          </a-entity>
        </a-camera>

        <a-light type="ambient" color="#fff" intensity="1" position="0 5 0"/>

        <a-entity id="oc-target">
          {children}
        </a-entity>

      </a-scene>
    )
  }
}

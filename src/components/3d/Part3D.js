import React from 'react'
import PropTypes from 'prop-types'
import {withProps} from '../../lib/decorators'
import autobind from 'autobind-decorator'

const EMPTY_SLOT = '<a-sphere radius=\'0.2\' opacity=\'0.2\' transparent color=\'#f00\'/>'

const interpolateModel = (part, allParts) =>
  Object.values(part.child || {}).reduce((a, c) =>
    a.replace(`{${c.name}}`, c.value ? interpolateModel(allParts[c.value], allParts) : EMPTY_SLOT), part.model)

@autobind
export default class Part3D extends React.Component {
  static propTypes = {
    parts: PropTypes.object,
    value: PropTypes.object,
    noChild: PropTypes.bool,
    axes: PropTypes.bool
  }

  @withProps
  render({parts, value = {}, axes, noChild}) {
    const {position = '0 0 0', rotation = '0 0 0'} = value

    return (
      <a-entity
        dangerouslySetInnerHTML={{__html: interpolateModel(value, parts)}}
        position={position}
        rotation={rotation}
        axis={axes}
      />
    )
  }
}

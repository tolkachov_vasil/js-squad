const partSide = 100

export const size = {
  part: {
    side: partSide,
    padding: partSide / 10,
    gap: partSide / 10,
    font: partSide / 10,
    slot: partSide / 2.5
  }
}

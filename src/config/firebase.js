import firebase from 'firebase'
import ReduxSagaFirebase from 'redux-saga-firebase'

const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyDE_0xT-nY00aPBufty5mDVTl_Aplfn-Bo',
  authDomain: 'js-bots.firebaseapp.com',
  databaseURL: 'https://js-bots.firebaseio.com',
  messagingSenderId: '825675540018'
})

// #IF DEBUG
// firebase.database.enableLogging(message => {
//   const data = message.replace(/^\w:[\d:]+\s/g, '')
//   const obj = data.startsWith('{') ? JSON.parse(data) : {message: data}
//   console.info('[FB]', obj)
// })
// #END

export const rsf = new ReduxSagaFirebase(firebaseApp)

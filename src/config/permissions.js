import {SCREEN} from './strings'

export const roles = {
  admin: {
    screens: [
      SCREEN.COMPOSER,
      SCREEN.EDITOR,
      SCREEN.MATERIALS,
      SCREEN.USERS
    ]
  },
  user: {
    screens: [
      SCREEN.COMPOSER
    ]
  }
}

const define = global.ace.define

define('ace/snippets/html', ['require', 'exports', 'module'], function (require, exports, module) {
  exports.snippetText = `
snippet entity
	<a-entity>
		&1
	</a-entity>
snippet box
	<a-box depth='&{1:1}' height='&{2:1}' width='&{3:1}'></a-box>
snippet camera
	<a-camera &1></a-camera>
snippet circle
	<a-circle radius='&{1:1}'></a-circle>
snippet collada-model
	<a-collada-model &1></a-collada-model>
snippet cone
	<a-cone height='&{1:1}' radius-bottom='&{2:1}' radius-top='&{3:0.8}'></a-cone>
snippet cursor
	<a-cursor &1></a-cursor>
snippet curvedimage
	<a-curvedimage &1></a-curvedimage>
snippet cylinder
	<a-cylinder height='&{1:1}' radius='&{2:1}'></a-cylinder>
snippet dodecahedron
	<a-dodecahedron &1></a-dodecahedron>
snippet gltf-model
	<a-gltf-model &1></a-gltf-model>
snippet icosahedron
	<a-icosahedron &1></a-icosahedron>
snippet image
	<a-image &1></a-image>
snippet light
	<a-light &1></a-light>
snippet link
	<a-link &1></a-link>
snippet obj-model
	<a-obj-model &1></a-obj-model>
snippet octahedron
	<a-octahedron &1></a-octahedron>
snippet plane
	<a-plane height='&{1:1}' width='&{2:1}'></a-plane>
snippet ring
	<a-ring radius-inner='&{1:0.8}' radius-outer='&{2:1.2}'></a-ring>
snippet sky
	<a-sky &1></a-sky>
snippet sound
	<a-sound &1></a-sound>
snippet sphere
	<a-sphere radius='&{1:1}'></a-sphere>
snippet tetrahedron
	<a-tetrahedron &1></a-tetrahedron>
snippet text
	<a-text &1></a-text>
snippet torus-knot
	<a-torus-knot &1></a-torus-knot>
snippet torus
	<a-torus radius='&{1:1}' radius-tubular='&{2:0.2}'></a-torus>
snippet triangle
	<a-triangle &1></a-triangle>
snippet video
	<a-video &1></a-video>
#
snippet position
	position='&{1:0} &{2:0} &{3:0}' &4
snippet rotation
	rotation='&{1:0} &{2:0} &{3:0}' &4
snippet scale
	scale='&{1:1} &{2:1} &{3:1}' &4
#
snippet material-steel
	material-steel &1
snippet material-steel
	material-steel &1
snippet material-glass
	material-glass &1
snippet material-plastic
	material-plastic &1
snippet material-rubber
	material-rubber &1
#
snippet rounded
	rounded='&{1:0.1}' &2
snippet color
	color='#&{1:700}' &2
snippet width
	width='&{1:1}' &2
snippet height
	height='&{1:1}' &2
snippet depth
	depth='&{1:1}' &2
snippet radius
	radius='&{1:1}' &2
snippet radius-tubular
	radius-tubular='&{1:0.1}' &2`
    .replace(/&/g, '$')

  exports.scope = 'html'
})

const ERROR = new Error('Too long path (limit is 4)')

const _remove = (obj, key) => {
  const {[key]: _, ...rest} = obj
  return rest
}

export const createReducer = (initialState, reducers) =>
  (state = initialState, action) =>
    (reducers[action.type] || (() => state))(state, action)

export const replace = (state, path, value) => {

  if (typeof path === 'string') {
    return {...state, [path]: value}
  } else {
    switch (path.length) {

      case 2:
        return {
          ...state,
          [path[0]]: {
            ...state[path[0]],
            [path[1]]: value
          }
        }

      case 3:
        return {
          ...state,
          [path[0]]: {
            ...state[path[0]],
            [path[1]]: {
              ...state[path[0]][path[1]],
              [path[2]]: value
            }
          }
        }

      case 4:
        return {
          ...state,
          [path[0]]: {
            ...state[path[0]],
            [path[1]]: {
              ...state[path[0]][path[1]],
              [path[2]]: {
                ...state[path[0]][path[1]][path[2]],
                [path[3]]: value
              }
            }
          }
        }

      default:
        throw ERROR
    }
  }
}

export const update = (state, path, value) => {

  if (typeof path === 'string') {
    return {...state, [path]: {...state[path], ...value}}
  } else {
    switch (path.length) {

      case 2:
        return {
          ...state,
          [path[0]]: {
            ...state[path[0]],
            [path[1]]: {...state[path[0]][path[1]], ...value}
          }
        }

      case 3:
        return {
          ...state,
          [path[0]]: {
            ...state[path[0]],
            [path[1]]: {
              ...state[path[0]][path[1]],
              [path[2]]: {...state[path[0]][path[1]][path[2]], ...value}
            }
          }
        }

      case 4:
        return {
          ...state,
          [path[0]]: {
            ...state[path[0]],
            [path[1]]: {
              ...state[path[0]][path[1]],
              [path[2]]: {
                ...state[path[0]][path[1]][path[2]],
                [path[3]]: {...state[path[0]][path[1]][path[2]][path[3]], ...value}
              }
            }
          }
        }

      default:
        throw ERROR
    }
  }
}

export const remove = (state, path) => {

  if (typeof path === 'string') {

    return _remove(state, path)

  } else {
    switch (path.length) {

      case 2:
        return {
          ...state,
          [path[0]]: _remove(state[path[0]], path[1])
        }

      case 3:
        return {
          ...state,
          [path[0]]: {
            ...state[path[0]],
            [path[1]]: _remove(state[path[0]][path[1]], path[2])
          }
        }

      case 4:
        return {
          ...state,
          [path[0]]: {
            ...state[path[0]],
            [path[1]]: {
              ...state[path[0]][path[1]],
              [path[2]]: _remove(state[path[0]][path[1]][path[2]], path[3])
            }
          }
        }

      default:
        throw ERROR
    }
  }
}


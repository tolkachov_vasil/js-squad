export function flattenDeep(array) {
  const result = []
  const nodes = array.slice()
  let node

  if (!array.length) {
    return result
  }

  node = nodes.pop()

  do {
    if (node instanceof Array) {
      nodes.push.apply(nodes, node)
    } else {
      result.push(node)
    }
  } while (nodes.length && (node = nodes.pop()) !== undefined)

  result.reverse()
  return result
}

export const isEmpty = val =>
  val === undefined || val === null || Object.keys(val).length === 0

export const reload = () =>
  window.location.reload()

export const hardReload = () => {
  localStorage.clear()
  reload()
}

export const withIndex = (obj, idKey = 'id') => {
  const result = {}
  Object.keys(obj).forEach(k => result[k] = {...obj[k], [idKey]: k})
  return result
}

export const indexedBy = (key, obj) => {
  const result = {}
  Object.values(obj).forEach(v => result[v[key]] = v)
  return result
}

export const isNoPrimitive = value => value !== null && typeof value === 'object'

export const getChangedFields = (o1 = {}, o2 = {}, result = {}) => {
  Object.keys({...o1, ...o2}).forEach(k => {
    if (isNoPrimitive(o1[k]) && isNoPrimitive(o2[k])) {
      result[k] = {}
      getChangedFields(o1[k], o2[k], result[k])
    } else if (o1[k] !== o2[k]) {
      result[k] = true
    }
  })

  return result
}

export const newId = () =>
  `${new Date().getTime()}`

export function attachPart(parts, parentId, childId, slotName) {
  const parent = parts[parentId]
  const child = parts[childId]
  const childStats = {...(child.stats || child.baseStats)}
  const updatedAt = new Date()

  parent.updatedAt = updatedAt
  child.updatedAt = updatedAt

  child.parent = parentId
  parent.child[slotName].value = childId

  if (!parent.stats) {
    parent.stats = {...parent.baseStats}
  }

  Object.keys(childStats).forEach(
    k => (parent.stats[k] = (parent.stats[k] || 0) + childStats[k])
  )
}

export function detachPart(parts, parentId, childId, slotName) {
  const parent = parts[parentId]
  const child = parts[childId]
  const childStats = {...(child.stats || child.baseStats)}
  const updatedAt = new Date()

  parent.updatedAt = updatedAt
  child.updatedAt = updatedAt

  child.parent = null
  parent.child[slotName].value = null

  Object.keys(childStats).forEach(
    k => (parent.stats[k] = parent.stats[k] - childStats[k])
  )
}

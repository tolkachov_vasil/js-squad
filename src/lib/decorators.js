export function withProps(target, name, descriptor) {
  return connected(descriptor, 'props', target)
}

export function withState(target, name, descriptor) {
  return connected(descriptor, 'state', target)
}

export function withPropsAndState(target, name, descriptor) {
  return connected(descriptor, 'all', target)
}

function connected(descriptor, key, target) {
  if (descriptor.value) {
    return connectedMethod(descriptor, key)
  }

  const decorator = key[0].toUpperCase() + key.slice(1)
  const className = target.constructor.name
  // eslint-disable-next-line
  throw `${className}: decorator @with${decorator} only for methods`
}

function connectedMethod(descriptor, key) {
  return key === 'all'
    ? {
      ...descriptor,
      value: function (...args) {
        return descriptor.value.apply(this, [this.props, this.state, ...args])
      }
    }
    : {
      ...descriptor,
      value: function (...args) {
        return descriptor.value.apply(this, [this[key], ...args])
      }
    }
}

import {getChangedFields} from '../utils'

describe('getChangedFields', () => {

  it('no changes', () => {

    expect(getChangedFields({
      node1: 1,
      node2: 2,
      node3: 'abcd'
    }, {
      node1: 1,
      node2: 2,
      node3: 'abcd'
    }))
      .toEqual({})
  })

  it('changes', () => {

    expect(getChangedFields({
      node1: 1,
      node2: null,
      node3: 3
    }, {
      node1: 11,
      node2: 'efgh',
      node3: 3
    }))
      .toEqual({
        node1: true,
        node2: true
      })
  })

  it('changes (add & remove fields)', () => {

    expect(getChangedFields({
      node1: 1,
      node2: 2,
      node3: 3
    }, {
      node2: 2,
      node3: 3,
      node4: 3
    }))
      .toEqual({
        node1: true,
        node4: true
      })
  })

  it('deep changes', () => {

    expect(getChangedFields({
      node1: 1,
      node2: {deep: {a: 1}},
      node3: 3
    }, {
      node1: 1,
      node2: {deep: {a: 11}},
      node3: 3
    }))
      .toEqual({
        node2: {deep: {a: true}}
      })
  })

})

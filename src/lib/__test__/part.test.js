import { attachPart, detachPart } from "../part"

let parts = {}

const init = {
  100: {
    parent: null,
    type: "body",
    name: "cup",
    baseStats: {
      shield: 5,
      weight: 5,
      size: 5
    },
    child: {
      inside: { type: "power", capacity: 1, value: null },
      top: { type: "camera", capacity: 1, value: null },
      bottom: { type: "chassis", capacity: 1, value: null }
    }
  },
  101: {
    parent: null,
    type: "chassis",
    name: "wheel",
    baseStats: {
      shield: 1,
      weight: 1,
      size: 1,
      power: 1
    }
  },
  102: {
    parent: null,
    type: "weapon",
    name: "laser",
    baseStats: {
      shield: 1,
      weight: 2,
      size: 3
    },
    child: {
      inside: { type: "power", capacity: 1, value: null }
    }
  },
  103: {
    parent: null,
    type: "power",
    name: "battery",
    baseStats: {
      shield: 1,
      weight: 5,
      size: 3
    }
  }
}

describe("robot", () => {
  beforeEach(() => (parts = JSON.parse(JSON.stringify(init))))

  it("insert", () => {
    attachPart(parts, 100, 101, "bottom")
    expect(parts).toEqual({
      "100": {
        baseStats: { shield: 5, size: 5, weight: 5 },
        child: {
          bottom: { capacity: 1, type: "chassis", value: 101 },
          inside: { capacity: 1, type: "power", value: null },
          top: { capacity: 1, type: "camera", value: null }
        },
        name: "cup",
        parent: null,
        stats: { power: 1, shield: 6, size: 6, weight: 6 },
        type: "body"
      },
      "101": {
        baseStats: { power: 1, shield: 1, size: 1, weight: 1 },
        name: "wheel",
        parent: 100,
        type: "chassis"
      },
      "102": {
        baseStats: { shield: 1, size: 3, weight: 2 },
        child: { inside: { capacity: 1, type: "power", value: null } },
        name: "laser",
        parent: null,
        type: "weapon"
      },
      "103": {
        baseStats: { shield: 1, size: 3, weight: 5 },
        name: "battery",
        parent: null,
        type: "power"
      }
    })
  })

  it("remove", () => {
    attachPart(parts, 100, 101, "bottom")
    detachPart(parts, 100, 101, "bottom")
    expect(parts).toEqual({
      "100": {
        baseStats: { shield: 5, size: 5, weight: 5 },
        child: {
          bottom: { capacity: 1, type: "chassis", value: null },
          inside: { capacity: 1, type: "power", value: null },
          top: { capacity: 1, type: "camera", value: null }
        },
        name: "cup",
        parent: null,
        stats: { power: 0, shield: 5, size: 5, weight: 5 },
        type: "body"
      },
      "101": {
        baseStats: { power: 1, shield: 1, size: 1, weight: 1 },
        name: "wheel",
        parent: null,
        type: "chassis"
      },
      "102": {
        baseStats: { shield: 1, size: 3, weight: 2 },
        child: { inside: { capacity: 1, type: "power", value: null } },
        name: "laser",
        parent: null,
        type: "weapon"
      },
      "103": {
        baseStats: { shield: 1, size: 3, weight: 5 },
        name: "battery",
        parent: null,
        type: "power"
      }
    })
  })
})

import {remove, replace, update} from '../reducer'

const state = {
  100: {
    parent: null,
    type: 'body',
    name: 'cup',
    child: {
      inside: {type: 'power', capacity: 1, value: null},
      top: {type: 'camera', capacity: 1, value: null},
      bottom: {type: 'chassis', capacity: 1, value: null}
    }
  }
}

describe('reducer', () => {

  it('replace state.field', () => {
    const newState = replace(state, '100', 'nope')
    expect(newState).toEqual({
      '100': 'nope'
    })
    expect(state !== newState).toEqual(true)
  })

  it('replace state.field1.field2', () => {
    const newState = replace(state, ['100', 'child'], 'nope')
    expect(newState).toEqual({
      '100': {
        'child': 'nope', 'name': 'cup', 'parent': null, 'type': 'body'
      }
    })
    expect(state !== newState).toEqual(true)
    expect(state['100'] !== newState['100']).toEqual(true)
  })

  it('replace state.field1.field2.field3', () => {
    const newState = replace(state, ['100', 'child', 'inside'], 'nope')
    expect(newState).toEqual({
      100: {
        parent: null,
        type: 'body',
        name: 'cup',
        child: {
          inside: 'nope',
          top: {type: 'camera', capacity: 1, value: null},
          bottom: {type: 'chassis', capacity: 1, value: null}
        }
      }
    })
    expect(state !== newState).toEqual(true)
    expect(state['100'] !== newState['100']).toEqual(true)
    expect(state['100'].child !== newState['100'].child).toEqual(true)
  })

  it('update state.field1.field2.field3', () => {
    const newState = update(state, ['100', 'child', 'inside'], {type: 'trash', value: 1})
    expect(newState).toEqual({
      100: {
        parent: null,
        type: 'body',
        name: 'cup',
        child: {
          inside: {type: 'trash', capacity: 1, value: 1},
          top: {type: 'camera', capacity: 1, value: null},
          bottom: {type: 'chassis', capacity: 1, value: null}
        }
      }
    })
  })

  it('remove state.field1', () => {
    const newState = remove(state, '100')
    expect(newState).toEqual({})
    expect(state !== newState).toEqual(true)
  })

  it('remove state.field1.field2', () => {
    const newState = remove(state, ['100', 'child'])
    expect(newState).toEqual({
      100: {
        parent: null,
        type: 'body',
        name: 'cup'
      }
    })
    expect(state !== newState).toEqual(true)
    expect(state['100'] !== newState['100']).toEqual(true)
  })

  it('remove state.field1.field2.field3', () => {
    const newState = remove(state, ['100', 'child', 'inside'])
    expect(newState).toEqual({
      100: {
        parent: null,
        type: 'body',
        name: 'cup',
        child: {
          top: {type: 'camera', capacity: 1, value: null},
          bottom: {type: 'chassis', capacity: 1, value: null}
        }
      }
    })
    expect(state !== newState).toEqual(true)
    expect(state['100'] !== newState['100']).toEqual(true)
    expect(state['100'].child !== newState['100'].child).toEqual(true)
  })

})

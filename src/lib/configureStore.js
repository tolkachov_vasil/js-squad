import {applyMiddleware, combineReducers, compose, createStore} from 'redux'
import createSagaMiddleware from 'redux-saga'
import sagas from '../redux/sagas/index'
import reducers from '../redux/reducers/index'
import {persistReducer, persistStore} from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const persistConfig = {
  key: 'root',
  storage
}

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware()
  const rootReducer = combineReducers(reducers)
  const persistedReducer = persistReducer(persistConfig, rootReducer)

  const store = compose(applyMiddleware(sagaMiddleware))(createStore)(
    persistedReducer,
    // #IF DEBUG
    window.__REDUX_DEVTOOLS_EXTENSION__
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : null
    // #END
  )

  const persistor = persistStore(store)

  sagaMiddleware.run(sagas)

  return {store, persistor}
}

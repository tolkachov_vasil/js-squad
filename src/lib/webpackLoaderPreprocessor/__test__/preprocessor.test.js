import {parse} from '../preprocessor'

describe('preprocessor', () => {

  it('no need parse', () => {
    const src = `
export const font = {
  fontFamily: "Arial"
}
`
    expect(parse(src)).toEqual(src)
  })

  it('parse DEBUG=true', () => {
    expect(parse([
      'export const font = {',
      '// #IF DEBUG',
      '  fontFamily: "Arial"',
      '// #ELSE',
      '  fontFamily: "Serif"',
      '// #END'].join('\n'), 'DEBUG=true')
    ).toEqual([
      'export const font = {',
      '// #IF DEBUG',
      '  fontFamily: "Arial"',
      '// #ELSE',
      '//  fontFamily: "Serif"',
      '// #END'].join('\n'))
  })

  it('parse DEBUG=false', () => {
    expect(parse([
      'export const font = {',
      '// #IF DEBUG',
      '  fontFamily: "Arial"',
      '// #ELSE',
      '  fontFamily: "Serif"',
      '// #END'].join('\n'), 'DEBUG=false')
    ).toEqual([
      'export const font = {',
      '// #IF DEBUG',
      '//  fontFamily: "Arial"',
      '// #ELSE',
      '  fontFamily: "Serif"',
      '// #END'].join('\n'))
  })

})

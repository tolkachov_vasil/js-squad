const NEED_PARSE = /(^|\n)\s*\/\/\s*#IF\s+\S+\s*(\n|$)/g
const DIRECTIVE_RX = /^\s*\/\/\s*(#IF|#ELSE|#END)(.+|$)/g

const OUTSIDE = 0
const INSIDE_THEN = 1
const INSIDE_ELSE = 2
let varsString
let isTrue
let state
let lines

function evaluate(cond) {
  return new Function(`{var ${varsString};return (${cond});}`)()
}

//need export for test
function parse(source, vars) {

  if (!NEED_PARSE.test(source)) {
    return source
  }

  lines = source.split('\n')
  const len = lines.length
  state = OUTSIDE
  varsString = vars

  for (let i = 0; i < len; i++) {
    const line = lines[i]
    const matchedDir = DIRECTIVE_RX.exec(line)

    if (matchedDir) {
      switchState(matchedDir)
    } else {
      processLine(i)
    }
  }

  shouldBeOutside()
  return lines.join('\n')
}

function switchState([_, dir, value]) {
  if (dir === '#IF') {
    shouldBeOutside()
    isTrue = !!evaluate(value, varsString)
    state = INSIDE_THEN
  } else if (dir === '#ELSE') {
    state = INSIDE_ELSE
  } else if (dir === '#END') {
    state = OUTSIDE
  }
}

function processLine(index) {
  if ((state === INSIDE_THEN && !isTrue) || (state === INSIDE_ELSE && isTrue)) {
    lines[index] = '//' + lines[index]
  }
}

function shouldBeOutside() {
  if (state !== OUTSIDE) {
    throw new Error('#END not found')
  }
}

// eslint-disable-next-line no-undef
exports.parse = parse



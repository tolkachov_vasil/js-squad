const preprocessor = require('./preprocessor')

module.exports = function (source, map) {
  try {
    const varsString = Object.keys(this.query).map(key => `${key}=${this.query[key]}`).join(';')
    source = preprocessor.parse(source, varsString)
    this.callback(null, source, map)
  }
  catch (err) {
    const errorMessage = 'preprocessor error: ' + err
    this.callback(new Error(errorMessage))
  }
}

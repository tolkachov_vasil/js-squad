import {call, put, take} from 'redux-saga/effects'
import {rsf} from '../config/firebase'
import {getChangedFields, isEmpty, isNoPrimitive} from './utils'
import {debugLog} from '../redux/actions/debug'

export function* createFB(path, value) {
  try {
    return yield call(rsf.database.create, path, value)
  } catch (error) {
    console.error('createFB', {path, value, error})
  }
}

export function* updateFB(path, value) {
  try {
    yield call(rsf.database.update, path, value)
  } catch (error) {
    console.error('updateFB', {path, value, error})
  }
}

export function* readFB(path) {
  try {
    return yield call(rsf.database.read, path)
  } catch (error) {
    console.error('readFB', {path, error})
  }
}

export function* listenFB(path, localValue, onChange) {
  const channel = yield call(rsf.database.channel, path)
  const isObj = isNoPrimitive(localValue)
  let local = isObj
    ? {...localValue}
    : localValue

  while (true) {
    const {value} = yield take(channel)
    const changed = isObj
      ? getChangedFields(local || {}, value || {})
      : value !== localValue

    if (!isEmpty(changed)) {
      // #IF DEBUG
      yield put(debugLog(path + ' : changed ' + Object.keys(changed).filter(k => changed[k]).join(', ')))
      // #END
      local = value
      yield onChange(changed, value)
    }
  }
}

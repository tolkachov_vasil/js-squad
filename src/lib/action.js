export function createAction(type, ...keys) {
  const creator = (...args) => {
    const action = {type}
    keys.forEach((_, i) => (action[keys[i]] = args[i]))
    // #IF DEBUG
    action.__FROM = getCaller()
    // #END
    return action
  }
  creator.type = type
  return creator
}

function getCaller(depth = 3) {
  let result = ''
  try {
    throw new Error()
  } catch (e) {
    result = e.stack.split(' at ').slice(depth)
  }
  return result
}

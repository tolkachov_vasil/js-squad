import React from 'react'
import {Provider} from 'react-redux'
import configureStore from './lib/configureStore'
import {PersistGate} from 'redux-persist/integration/react'
import {withState} from './lib/decorators'
import Auth from './components/Auth'
import Screen from './screens/index'
import {css, StyleSheet} from 'aphrodite'

export default class App extends React.Component {
  state = configureStore()

  @withState
  render({store, persistor}) {
    return (
      <div className={css(styles.root)}>
        <Provider store={store}>
          <PersistGate
            loading={null}
            persistor={persistor}
          >
            <Auth>
              <Screen/>
            </Auth>
          </PersistGate>
        </Provider>
      </div>
    )
  }
}

const styles = StyleSheet.create({
  root: {}
})

import React from 'react'
import ReactDOM from 'react-dom'
// import 'reset-css'
import './root-styles.css'
import './init'
import App from './App'
import registerServiceWorker from './registerServiceWorker'

// #IF DEBUG
console.log('--- DEVELOPMENT MODE ---')
// #ELSE
console.log('--- PRODUCTION MODE ---')
// #END

ReactDOM.render(<App/>, document.getElementById('root'))
registerServiceWorker()

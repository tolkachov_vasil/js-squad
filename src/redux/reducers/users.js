import {createReducer, replace} from '../../lib/reducer'
import {usersLoadSuccess, usersUpdateOne} from '../actions/users'

export default createReducer(
  {},
  {
    [usersLoadSuccess.type]: (s, {users}) => users,

    [usersUpdateOne.type]: (s, {user}) => replace(s, user.id, user)
  }
)

import {createReducer, replace} from '../../lib/reducer'
import {versionUpdate, versionUpdateField} from '../actions/version'

export default createReducer(
  {
    parts: null,
    materials: null,
    users: null
  },
  {
    [versionUpdate.type]: (s, {value}) => value,

    [versionUpdateField.type]: (s, {field, value}) => replace(s, field, value)
  }
)

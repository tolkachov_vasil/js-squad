import {createReducer, replace} from '../../lib/reducer'
import {boardUpdate, boardUpdateItem} from '../actions/board'

const width = 20
const height = 20
export const EMPTY = {}

export default createReducer(
  {
    width,
    height,
    items: new Array(height).fill(1).map(() => new Array(width).fill(EMPTY))
  },
  {
    [boardUpdate.type]: (s, {data}) => data,

    [boardUpdateItem.type]: (s, {x, y, value}) => replace(s, ['items', y], value)
  }
)

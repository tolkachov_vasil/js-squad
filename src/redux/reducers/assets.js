import {assetsDeletePart, assetsUpdateField, assetsUpdateMaterials, assetsUpdatePart} from '../actions/assets'
import {createReducer, remove, replace} from '../../lib/reducer'

export default createReducer(
  {
    parts: {},
    materials: {}
  },
  {
    [assetsUpdateField.type]: (s, {field, value}) => replace(s, field, value),

    [assetsUpdatePart.type]: (s, {part}) => replace(s, ['parts', part.id], part),

    [assetsDeletePart.type]: (s, {id}) => remove(s, ['parts', id]),

    [assetsUpdateMaterials.type]: (s, {materials}) => replace(s, 'materials', materials)
  }
)

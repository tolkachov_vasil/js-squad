import {userLoadProfileSuccess, userLoginSuccess, userUpdateField} from '../actions/user'
import {createReducer, replace} from '../../lib/reducer'

export default createReducer(
  {
    loading: false,
    logged: false,
    id: null,// 'QBDHvTzY44T0ZnVRIRcPwQYXPQ82',
    loaded: false,
    role: 'user'
  },
  {
    [userUpdateField.type]: (s, {field, value}) => replace(s, field, value),

    [userLoginSuccess.type]: (s, {data}) => ({...s, loading: false, logged: true, ...data}),

    [userLoadProfileSuccess.type]: (s, {data}) => ({...s, loaded: true, loading: false, ...data})
  }
)

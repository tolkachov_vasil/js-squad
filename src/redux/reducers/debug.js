import {createReducer} from '../../lib/reducer'
import {debugLog} from '../actions/debug'

export default createReducer(
  {},
  {
    [debugLog.type]: (s, {message}) => console.log(message) || s
  }
)

import select from './select'
import parts from './parts'
import assets from './assets'
import user from './user'
import users from './users'
import version from './version'
// import robots from './robots'
import board from './board'
// import event from './event'
// #IF DEBUG
import debug from './debug'
// #END

export default {
  assets,
  select,
  parts,
  user,
  users,
  version,
  board,
  // #IF DEBUG
  debug
  // #END
}

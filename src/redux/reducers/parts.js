import {partsSaveOne, partsUpdateOne, partsUpdate} from '../actions/parts'
import {createReducer, replace} from '../../lib/reducer'

export default createReducer(
  {},
  {
    [partsUpdate.type]: (_, {parts}) => parts,

    [partsUpdateOne.type]: (s, {part}) => replace(s, part.id, part),

    [partsSaveOne.type]: (s, {part}) => replace(s, part.id, part)
  }
)

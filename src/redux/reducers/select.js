import {selectPart, selectScreen, selectSlot, selectUser} from '../actions/select'
import {createReducer, replace} from '../../lib/reducer'
import {SCREEN} from '../../config/strings'

export default createReducer(
  {
    part: null,
    slot: null,
    screen: SCREEN.COMPOSER,
    user: null
  },
  {
    [selectPart.type]: (s, {part}) => replace(s, 'part', part),

    [selectSlot.type]: (s, {part, name}) => replace(s, 'slot', {part, name}),

    [selectScreen.type]: (s, {name}) => replace(s, 'screen', name),

    [selectUser.type]: (s, {id}) => replace(s, 'user', id)
  }
)

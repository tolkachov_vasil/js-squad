import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {partsAttach, partsDetach, partsSave, partsUpdate} from '../actions/parts'
import {attachPart, detachPart} from '../../lib/part'
import {selectPart} from '../actions/select'
import merge from 'deepmerge'
import {updateFB} from '../../lib/saga-firebase'

function* attach({target, part, slot}) {
  const parts = yield select(s => s.parts)
  attachPart(parts, target, part, slot)
  yield put(partsUpdate({...parts}))
  yield put(selectPart(parts[target].id))
}

function* detach({source, part, slot}) {
  const parts = yield select(s => s.parts)
  detachPart(parts, source, part, slot)
  yield put(partsUpdate({...parts}))
}

function* saveMyParts() {
  const parts = yield select(s => s.parts)
  const {uid} = yield select(s => s.user)
  const compressedParts = compressParts(parts)

  yield call(updateFB, `/users/${uid}/parts`, compressedParts)
}

function compressParts(parts) {
  return Object.values(parts).reduce((a, part) => {
    const {id, originId, child, parent = null} = part

    const newChild = child
      ? Object.values(child).reduce((a, {name, value}) => value ? ({...a, [name]: {value}}) : null, {})
      : null

    return {...a, [id]: {id, originId, child: newChild, parent}}
  }, {})
}

export function* updateParts(parts, originalParts) {
  const userParts = {}

  Object.keys(parts).forEach(k =>
    userParts[k] = merge(originalParts[parts[k].originId], {...parts[k], id: k}))

  yield put(partsUpdate(userParts))
}

export default function* () {
  yield all([
    takeLatest(partsAttach.type, attach),
    takeLatest(partsDetach.type, detach),
    takeLatest(partsSave.type, saveMyParts)
  ])
}

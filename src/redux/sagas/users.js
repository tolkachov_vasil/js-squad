import {all, call, put, takeLatest} from 'redux-saga/effects'
import {
  usersAddPartToOne,
  usersLoadRequest,
  usersLoadSuccess,
  usersSaveOneRequest,
  usersUpdateOne
} from '../actions/users'
import {withIndex} from '../../lib/utils'
import {createFB, readFB, updateFB} from '../../lib/saga-firebase'

export function* loadUsers() {
  const users = yield readFB('users')
  yield put(usersLoadSuccess(withIndex(users)))
}

function* saveUser({user}) {
  yield call(updateFB, `/users/${user.id}`, user)
}

function* addUserPart({user, part}) {
  const userPart = {originId: part.id}

  const id = yield call(createFB, `/users/${user.id}/parts`, userPart)
  const userParts = {...(user.parts || {}), [id]: userPart}

  yield put(usersUpdateOne({...user, parts: userParts}))
}

export default function* () {
  yield all([
    takeLatest(usersLoadRequest.type, loadUsers),
    takeLatest(usersSaveOneRequest.type, saveUser),
    takeLatest(usersAddPartToOne.type, addUserPart)
  ])
}

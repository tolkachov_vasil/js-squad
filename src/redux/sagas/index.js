import {all} from 'redux-saga/effects'
import parts from './parts'
import assets from './assets'
import user from './user'
import users from './users'
import version from './version'

export default function* rootSaga() {
  yield all([
    assets(),
    user(),
    users(),
    parts(),
    version()
  ])
}

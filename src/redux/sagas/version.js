import {all, call, fork, put, select, takeLatest} from 'redux-saga/effects'
import {loadMaterials, loadParts} from './assets'
import {versionUpdate} from '../actions/version'
import {updateParts} from './parts'
import {listenFB, readFB} from '../../lib/saga-firebase'
import {loadUsers} from './users'
import {userUpdateField} from '../actions/user'

function* init() {
  const {logged, id, version: userVersion} = yield select(s => s.user)
  const rootVersion = yield select(s => s.version)

  if (logged) {
    yield fork(listenFB, 'version', rootVersion, updateRootVersion)
    yield fork(listenFB, `users/${id}/version`, userVersion, updateUserVersion)
  }
}

function* updateRootVersion(changed, newValue) {
  if (changed.materials) {
    yield loadMaterials()
  }
  if (changed.parts) {
    yield loadParts()
    const parts = yield select(s => s.parts)
    yield mergeUserParts(parts)
  }
  if (changed.users) {
    const {role} = yield select(s => s.user)

    if (role === 'admin') {
      yield loadUsers()
    }
  }
  yield put(versionUpdate(newValue))
}

function* updateUserVersion(changed, newValue) {
  if (changed.parts) {
    const {id} = yield select(s => s.user)
    const parts = yield call(readFB, `users/${id}/parts`)
    yield mergeUserParts(parts)
  }
  yield put(userUpdateField('version', newValue))
}

function* mergeUserParts(userParts = {}) {
  const {parts = {}} = yield select(s => s.assets)
  yield updateParts(userParts, parts)
}

export default function* () {
  yield all([
    takeLatest('persist/REHYDRATE', init)
  ])
}

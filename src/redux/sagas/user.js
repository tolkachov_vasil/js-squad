import firebase from 'firebase'
import {rsf} from '../../config/firebase'
import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {
  userLoadProfileSuccess,
  userLoginFail,
  userLoginRequest,
  userLoginSuccess,
  userLogoutRequest,
  userLogoutSuccess
} from '../actions/user'
import {hardReload, reload} from '../../lib/utils'
import {readFB} from '../../lib/saga-firebase'

const authProvider = new firebase.auth.GoogleAuthProvider()

function* presenseHandler(id) {
  const sharedOnlineRef = firebase.database().ref(`shared/online/${id}`)
  const myConnectionsNumber = yield call(rsf.database.read, sharedOnlineRef) || 0

  sharedOnlineRef.set(myConnectionsNumber + 1)
  sharedOnlineRef.onDisconnect().set(myConnectionsNumber || null)
}

function* init() {
  const {id, logged, loaded} = yield select(s => s.user)

  if (id) {
    yield presenseHandler(id)
  }

  if (logged && !loaded) {
    yield loadProfile()
  }
}

function* login() {
  try {

    yield call(rsf.auth.signInWithPopup, authProvider)
    const {id, displayName, email} = firebase.auth().currentUser
    yield put(userLoginSuccess({id, displayName, email}))
    yield loadProfile()
    reload()

  } catch (error) {
    console.error('login', error)
    yield put(userLoginFail(error))
  }
}

function* logout() {
  try {

    yield call(rsf.auth.signOut)
    yield put(userLogoutSuccess())
    hardReload()

  } catch (error) {
    console.error('logout', error)
    yield put(userLoginFail(error))
  }
}

export function* loadProfile() {
  const {id} = yield select(s => s.user)

  const profile = yield call(readFB, `/users/${id}`)

  if (profile) {
    yield put(userLoadProfileSuccess(profile))
  } else {
    console.error('loadProfile error')
  }
}

export default function* () {
  yield all([
    takeLatest('persist/REHYDRATE', init),
    takeLatest(userLoginRequest.type, login),
    takeLatest(userLogoutRequest.type, logout)
  ])
}

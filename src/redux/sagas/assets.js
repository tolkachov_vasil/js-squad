import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {rsf} from '../../config/firebase'
import {
  assetsCreateMaterial,
  assetsCreatePart,
  assetsDeletePart,
  assetsSaveMaterialsRequest,
  assetsSavePartRequest,
  assetsUpdateField,
  assetsUpdatePart
} from '../actions/assets'
import {registerMaterials} from '../../init/register-materials'
import {partsUpdate, partsUpdateOne} from '../actions/parts'
import merge from 'deepmerge'
import {isEmpty} from '../../lib/utils'
import {readFB} from '../../lib/saga-firebase'

function* init() {
  const {materials} = yield select(s => s.assets)

  yield registerMaterials(materials)
}

function* savePart({part}) {
  yield syncPart(part)
}

function* refreshMyPart(part) {
  const {id} = part
  const parts = yield select(s => s.parts)
  const myPart = Object.values(parts).find(p => p.originId === id)

  if (myPart) {
    const merged = merge(myPart, part)
    merged.id = myPart.id
    yield put(partsUpdateOne(merged))
  }
}

function* syncPart(part) {
  const {id, child, model = ''} = part
  part.child = child ? Object.values(part.child).reduce((a, c) => ({...a, [c.name]: c}), {}) : null
  part.model = model.replace(/\s{2,}/g, ' ').replace(/\n/g, '')
  part.originId = null

  try {
    yield call(rsf.database.update, `/parts/${id}`, part)
    yield refreshMyPart(part)

  } catch (error) {
    console.log('error', error)
  }
}

function* create(part) {
  const {id} = part
  try {
    yield call(rsf.database.patch, '/parts', {[id]: part})
    yield refreshMyPart(part)

  } catch (error) {
    console.log('error', error)
  }
}

function* createPart() {
  const {parts} = yield select(s => s.assets)
  const id = 'id_' + (Math.max(...Object.keys(parts).map(k => +k.replace(/^id_/, ''))) + 1)
  const newPart = {
    id,
    baseStats: {
      shield: 1,
      size: 1,
      weight: 1
    },
    name: 'part_' + id,
    type: 'type',
    model: '<a-entity></a-entity>'
  }
  yield create(newPart)
  yield put(assetsUpdatePart(newPart))
}

function* deletePart({id}) {
  const parts = yield select(s => s.parts)
  const updated = {...parts}

  Object.values(parts).forEach(p => {
    if (p.originId === id) {
      delete updated[p.id]
    }
  })

  Object.values(parts).forEach(({child = {}, id}) => {
    Object.values(child).forEach(({value}) => {
      if (value && !updated[value]) {
        delete updated.child[id]
      }
    })
  })
  yield put(partsUpdate(updated))

  try {
    yield call(rsf.database.delete, `/parts/${id}`)
  } catch (error) {
    console.log('error', error)
  }
}

function* saveMaterials() {
  const {materials} = yield select(s => s.assets)

  const normalized = Object.values(materials).reduce((a, mat) =>
    ({...a, [mat.name]: mat}), {})

  try {
    yield call(rsf.database.update, `/materials`, normalized)
  } catch (error) {
    console.log('error', error)
  }
}

function* createMaterial() {
  const {materials} = yield select(s => s.assets)
  const name = 'material_' + Object.keys(materials).length

  yield put(assetsUpdateField('materials', {...materials, [name]: {name}}))
  yield saveMaterials()
}

export function* updateParts(parts, originalParts) {
  const userParts = {}

  Object.keys(parts).forEach(k =>
    userParts[k] = merge({...parts[k], id: k}, originalParts[parts[k].originId]))

  yield put(partsUpdate(userParts))

}

export function* loadParts() {
  const originalParts = yield call(readFB, '/parts')
  yield put(assetsUpdateField('parts', originalParts))
  const parts = yield select(s => s.parts)

  !isEmpty(parts) && updateParts(parts, originalParts)

}

export function* loadMaterials() {
  const materials = yield call(readFB, '/materials')
  yield put(assetsUpdateField('materials', materials))

  registerMaterials(materials)
}

export default function* () {
  yield all([
    takeLatest('persist/REHYDRATE', init),

    takeLatest(assetsSavePartRequest.type, savePart),
    takeLatest(assetsCreatePart.type, createPart),
    takeLatest(assetsDeletePart.type, deletePart),

    takeLatest(assetsSaveMaterialsRequest.type, saveMaterials),
    takeLatest(assetsCreateMaterial.type, createMaterial)
    // takeLatest(ASSETS.MATERIAL.DELETE, deleteMaterial)
    // takeLatest('persist/REHYDRATE', listenPartsUpdates),
    // takeLatest('persist/REHYDRATE', listenMaterialsUpdates)
  ])
}

import {createAction} from '../../lib/action'

export const selectPart = createAction('selectPart', 'part')
export const selectSlot = createAction('selectSlot', 'partId', 'slotName')
export const selectScreen = createAction('selectScreen', 'name')
export const selectUser = createAction('selectUser', 'id')

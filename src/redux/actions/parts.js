import {createAction} from '../../lib/action'

export const partsAttach = createAction('partsAttach', 'target', 'part', 'slot')
export const partsDetach = createAction('partsDetach', 'source', 'part', 'slot')

export const partsLoadRequest = createAction('partsLoadRequest')
export const partsLoadSuccess = createAction('partsLoadSuccess', 'parts')

export const partsUpdate = createAction('partsUpdate', 'parts')
export const partsUpdateOne = createAction('partsUpdateOne', 'part')
export const partsSaveOne = createAction('partsSaveOne', 'part') //todo request
export const partsSave = createAction('partsSave') //todo request

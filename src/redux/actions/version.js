import {createAction} from '../../lib/action'

export const versionUpdate = createAction('versionUpdate',  'value')
export const versionUpdateField = createAction('versionUpdateField', 'field', 'value')


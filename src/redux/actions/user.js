import {createAction} from '../../lib/action'

export const userUpdateField = createAction('userUpdateField','field','value')

export const userLoginRequest = createAction('userLoginRequest')
export const userLoginSuccess = createAction('userLoginSuccess', 'data')
export const userLoginFail = createAction('userLoginFail')

export const userLogoutRequest = createAction('userLogoutRequest')
export const userLogoutSuccess = createAction('userLogoutSuccess')

export const userLoadProfileRequest = createAction('userLoadProfileRequest')
export const userLoadProfileSuccess = createAction('userLoadProfileSuccess', 'data')

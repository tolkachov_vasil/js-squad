import {createAction} from '../../lib/action'

export const assetsInit = createAction('assetsInit')

export const assetsUpdateField = createAction('assetsUpdateField', 'field', 'value')

export const assetsUpdatePart = createAction('assetsUpdatePart', 'part')
export const assetsCreatePart = createAction('assetsCreatePart')
export const assetsDeletePart = createAction('assetsDeletePart', 'id')

export const assetsSavePartRequest = createAction('assetsSavePartRequest', 'part')

export const assetsUpdateMaterials = createAction('assetsUpdateMaterials', 'materials')
export const assetsCreateMaterial = createAction('assetsCreateMaterial')
export const assetsDeleteMaterial = createAction('assetsDeleteMaterial', 'name')

export const assetsSaveMaterialsRequest = createAction('assetsSaveMaterialsRequest')

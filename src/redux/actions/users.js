import {createAction} from '../../lib/action'

export const usersLoadRequest = createAction('usersLoadRequest')
export const usersLoadSuccess = createAction('usersLoadSuccess', 'users')

export const usersUpdateOne = createAction('usersUpdateOne', 'user')
export const usersAddPartToOne = createAction('usersAddPartToOne', 'user', 'part')
export const usersSaveOneRequest = createAction('usersSaveOneRequest', 'user')

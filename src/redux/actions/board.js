import {createAction} from '../../lib/action'

export const boardUpdate = createAction('boardUpdate', 'data')

export const boardUpdateItem = createAction('boardUpdateItem', 'x', 'y', 'value')

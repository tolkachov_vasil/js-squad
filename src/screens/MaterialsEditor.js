import React from 'react'
import {withProps, withPropsAndState} from '../lib/decorators'
import {css, StyleSheet} from 'aphrodite'
import {connect} from 'react-redux'
import autobind from 'autobind-decorator'
import {
  assetsCreateMaterial,
  assetsDeleteMaterial,
  assetsSaveMaterialsRequest,
  assetsUpdateMaterials
} from '../redux/actions/assets'
import MaterialEdit from '../components/MaterialEdit'

@connect(
  ({assets}) => ({assets}),
  {assetsUpdateMaterials, assetsSaveMaterialsRequest, assetsCreateMaterial, assetsDeleteMaterial})
@autobind
export default class MaterialsEditor extends React.Component {

  state = {name: null}

  select({target}) {
    const name = target.getAttribute('data-name')
    this.setState({name})
  }

  @withProps
  delete({assetsDeleteMaterial}, name) {
    this.setState({name: null})
    assetsDeleteMaterial(name)
  }

  @withProps
  update({assets: {materials}, assetsUpdateMaterials}, value) {
    assetsUpdateMaterials({...materials, [value.name]: value})
  }

  @withPropsAndState
  render({assets: {materials}, updateMaterial, assetsSaveMaterialsRequest, assetsCreateMaterial, assetsDeleteMaterial}, {name}) {
    const material = materials[name]

    return (
      <div className={css(styles.root)}>

        {Object.values(materials).map(({name}) =>
          <button key={name} data-name={name} onClick={this.select}>{name}</button>)}

        <button onClick={assetsCreateMaterial}>+ add new</button>

        <hr/>

        {material &&
         <MaterialEdit
           value={material}
           onChange={this.update}
           onSave={assetsSaveMaterialsRequest}
           onDelete={this.delete}
         />}

      </div>
    )
  }
}

const styles = StyleSheet.create({
  root: {}
})

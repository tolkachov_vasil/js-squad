import React from 'react'
import {withProps} from '../lib/decorators'
import {connect} from 'react-redux'
import PartsEditor from './PartsEditor'
import Composer from './Composer'
import MaterialsEditor from './MaterialsEditor'
import {roles} from '../config/permissions'
import {selectScreen} from '../redux/actions/select'
import autobind from 'autobind-decorator'
import Users from './Users'
import {SCREEN} from '../config/strings'

const screenComponents = {
  [SCREEN.EDITOR]: <PartsEditor/>,
  [SCREEN.MATERIALS]: <MaterialsEditor/>,
  [SCREEN.COMPOSER]: <Composer/>,
  [SCREEN.USERS]: <Users/>
}

@connect(({select, user}) => ({select, user}), {selectScreen})
@autobind
export default class Screen extends React.Component {

  @withProps
  selectScreen({selectScreen}, {target}) {
    selectScreen(target.getAttribute('data-name'))
  }

  render() {
    return (
      <div>
        <hr/>
        {this.renderNavigation()}
        <hr/>
        {this.renderScreens()}
      </div>
    )
  }

  @withProps
  renderNavigation({user: {role}}) {
    return roles[role].screens.map(name =>
      <button onClick={this.selectScreen} data-name={name} key={name}> {name} </button>)
  }

  @withProps
  renderScreens({select: {screen}}) {
    return screenComponents[screen]
  }
}

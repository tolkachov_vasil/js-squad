import React from 'react'
import {withProps} from '../lib/decorators'
import {css, StyleSheet} from 'aphrodite'
import Part3D from '../components/3d/Part3D'
import {connect} from 'react-redux'
import Parts from '../components/Parts'
import {partsSave} from '../redux/actions/parts'
import {View3D} from '../components/3d/View3D'

@connect(({select, parts}) => ({select, parts}), {partsSave})
export default class Composer extends React.Component {

  @withProps
  render({select, parts, partsSave}) {
    const selectedId = select.partId
    const part = selectedId ? parts[selectedId] : null

    return (
      <div className={css(styles.root)}>
        <Parts/>
        <button onClick={partsSave}>save</button>
        <View3D>
          {part &&
           <Part3D parts={parts} value={part}/>}
        </View3D>
      </div>
    )
  }
}

const styles = StyleSheet.create({
  root: {}
})

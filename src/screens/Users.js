import PropTypes from 'prop-types'
import React from 'react'
import {withProps, withPropsAndState} from '../lib/decorators'
import {css, StyleSheet} from 'aphrodite'
import {connect} from 'react-redux'
import autobind from 'autobind-decorator'
import {usersAddPartToOne, usersLoadRequest, usersSaveOneRequest, usersUpdateOne} from '../redux/actions/users'
import {selectUser} from '../redux/actions/select'

@connect(({select, users, assets}) => ({select, users, assets}),
  {usersLoadRequest, usersSaveOneRequest, usersUpdateOne, selectUser, usersAddPartToOne})
@autobind
export default class Users extends React.Component {
  static propTypes = {
    select: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired
  }

  state = {addPartActive: false}

  get selectedUser() {
    const {select: {user}, users} = this.props
    return user ? users[user] : null
  }

  @withProps
  selectUser({selectUser}, {target}) {
    selectUser(target.getAttribute('data-name'))
  }

  startAddPart() {
    this.setState({addPartActive: true})
  }

  @withProps
  addPart({usersAddPartToOne, assets: {parts}}, {target}) {
    const user = this.selectedUser
    const partId = target.getAttribute('data-id')
    const part = parts[partId]

    usersAddPartToOne(user, part)
    this.setState({addPartActive: false})
  }

  @withProps
  saveUser({usersSaveOneRequest}) {
    const user = this.selectedUser
    user && usersSaveOneRequest(user)
  }

  render() {
    return (
      <div className={css(styles.root)}>
        {this.renderUsers()}
        <hr/>
        {this.renderUser()}
      </div>
    )
  }

  @withProps
  renderUsers({users}) {
    return Object.values(users).map(({id, displayName}) =>
      <button
        key={id}
        data-name={id}
        onClick={this.selectUser}
      >
        {displayName}
      </button>)
  }

  renderUser() {
    const user = this.selectedUser

    if (!user) {
      return null
    }

    const {displayName, email, role, parts = {}} = user

    return (
      <div>
        name: {displayName}<br/>
        email: {email}<br/>
        role: {role}<br/>
        {this.renderParts(parts)}
        <button onClick={this.saveUser}>save</button>
      </div>
    )
  }

  @withPropsAndState
  renderParts({assets}, {addPartActive}, parts) {
    return (
      <div>
        parts: {Object.keys(parts).map(k => <div key={k}>{assets.parts[parts[k].originId].name}</div>)}

        {addPartActive ?
          Object.values(assets.parts).map(p =>
            <button
              key={p.id}
              onClick={this.addPart}
              data-id={p.id}
            >
              {p.name}
            </button>)
          :
          <button onClick={this.startAddPart}>+ add part</button>}

      </div>
    )
  }
}

const styles = StyleSheet.create({
  root: {}
})

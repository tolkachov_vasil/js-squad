import React from 'react'
import {withProps, withPropsAndState} from '../lib/decorators'
import {css, StyleSheet} from 'aphrodite'
import {connect} from 'react-redux'
import autobind from 'autobind-decorator'
import PartEdit from '../components/PartEdit'
import {assetsCreatePart, assetsDeletePart, assetsSavePartRequest, assetsUpdatePart} from '../redux/actions/assets'

@connect(
  ({assets}) => ({assets}),
  {assetsUpdatePart, assetsSavePartRequest, assetsCreatePart, assetsDeletePart})
@autobind
export default class PartsEditor extends React.Component {

  state = {id: null}

  select({target}) {
    const id = target.getAttribute('data-id')
    this.setState({id})
  }

  @withProps
  delete({assetsDeletePart}, id) {
    this.setState({id: null})
    assetsDeletePart(id)
  }

  @withPropsAndState
  render({assets: {parts}, assetsUpdatePart, assetsSavePartRequest, assetsCreatePart, assetsDeletePart}, {id}) {
    const part = parts[id]

    return (
      <div className={css(styles.root)}>

        {Object.values(parts).map(({id, name}) =>
          <button key={id} data-id={id} onClick={this.select}>{name}</button>)}

        <button onClick={assetsCreatePart}>+ add new</button>

        <hr/>

        {part &&
         <PartEdit
           part={part}
           onChange={assetsUpdatePart}
           onSave={assetsSavePartRequest}
           onDelete={this.delete}
         />}

      </div>
    )
  }
}

const styles = StyleSheet.create({
  root: {}
})

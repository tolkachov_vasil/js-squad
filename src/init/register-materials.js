import * as AFRAME from 'aframe'

export const registerMaterials = materials =>
  Object.values(materials).forEach(material => {
      const {name, ...attrs} = material

      try {
        AFRAME.registerComponent('material-' + name, {
          schema: {type: 'string'},
          init: function () {
            Object.keys(attrs).forEach(attrName =>
              this.el.setAttribute(attrName, material[attrName]))
          }
        })
      } catch (e) {
      }
    }
  )

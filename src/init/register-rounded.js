import * as AFRAME from 'aframe'

const THREE = AFRAME.THREE

function createBoxWithRoundedEdges(width, height, depth, radius0, smoothness) {
  const shape = new THREE.Shape()
  const eps = 0.00001
  const radius = radius0 - eps
  shape.absarc(eps, eps, eps, -Math.PI / 2, -Math.PI, true)
  shape.absarc(eps, height - radius * 2, eps, Math.PI, Math.PI / 2, true)
  shape.absarc(width - radius * 2, height - radius * 2, eps, Math.PI / 2, 0, true)
  shape.absarc(width - radius * 2, eps, eps, 0, -Math.PI / 2, true)

  const geometry = new THREE.ExtrudeBufferGeometry(shape, {
    amount: depth - radius0 * 2,
    bevelEnabled: true,
    bevelSegments: smoothness * 2,
    steps: 1,
    bevelSize: radius,
    bevelThickness: radius0,
    curveSegments: smoothness
  })

  geometry.center()
  return geometry
}

function createCylinderWithRoundedEdges(radius, height, radius0, smoothness, segments) {
  const eps = 0.00001
  const radius1 = radius0 - eps
  const radius2 = radius - radius0
  const shape = new THREE.Shape()
  shape.autoClose = true
  shape.absarc(0, 0, radius2, 0.01, Math.PI * 2, false, 0)

  const geometry = new THREE.ExtrudeBufferGeometry(shape, {
    amount: height - radius0 * 2,
    bevelEnabled: true,
    bevelSegments: smoothness,
    steps: 1,
    bevelSize: radius1,
    bevelThickness: radius0,
    curveSegments: segments || smoothness
  })

  geometry.center()
  return geometry
}

AFRAME.registerComponent('rounded', {
  schema: {type: 'string'},

  init: function () {
    setTimeout(() => {

      const el = this.el
      const isBox = el.tagName === 'A-BOX'
      const isCylinder = el.tagName === 'A-CYLINDER'
      const data = el.attributes
      const attr = {}

      for (let i = data.length - 1; i >= 0; i--) {
        if (data[i].value) {
          attr[data[i].name] = data[i].value
        }
      }

      const {color = '#400', opacity = 1, metalness = 0.5, roughness = 0.5} = attr

      const material = new THREE.MeshStandardMaterial({
        opacity: +opacity,
        color,
        metalness,
        roughness,
        transparent: +opacity < 1
      })

      let mesh

      if (isBox) {
        const {width = 1, height = 1, depth = 1, rounded = 0.1} = attr
        const geometry = createBoxWithRoundedEdges(width, height, depth, rounded, 8)
        mesh = new THREE.Mesh(geometry, material)

      } else if (isCylinder) {
        const {radius = 1, height = 1, rounded = 0.1} = attr
        const segments = attr['segments-radial']
        const geometry = createCylinderWithRoundedEdges(radius, height, rounded, segments || 32, segments / 2)
        mesh = new THREE.Mesh(geometry, material)
        mesh.rotation.x = Math.PI / 2

      } else {
        return
      }

      el.setObject3D('mesh', mesh)

    })
  }
})
